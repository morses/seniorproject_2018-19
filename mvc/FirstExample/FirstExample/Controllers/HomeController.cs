﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstExample.Controllers
{
    public class HomeController : Controller
    {
        // Controller Action Method
        public ActionResult Index()
        {
            return View();
        }

        public string Hello()
        {
            return "Hello World";
        }

        public ActionResult MileConverter()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}